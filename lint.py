#!/usr/bin/env python

import subprocess
from argparse import ArgumentParser

FILES = ["src", "tests", "lint.py"]

if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="Run some automatic code prettifying and linting on the sources."
    )
    arg_parser.add_argument(
        "--check",
        action="store_true",
        help="Do not try to repair, just check all is well.",
    )
    args = arg_parser.parse_args()

    if args.check:
        subprocess.run(["isort", "--check", *FILES], check=True)
    else:
        subprocess.run(["isort", *FILES], check=True)

    subprocess.run(["black", *FILES], check=True)

    subprocess.run(["flake8", "--max-line-length=120", *FILES], check=True)

    subprocess.run(["mypy", "--strict", *FILES], check=True)
