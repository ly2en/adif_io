# Contributing

## How to lint and test

Each of the following commands should work nicely, both before and
after any change you might want to contribute:

```
python -m venv venv
. venv/bin/activate
pip install -r requirements-lint+test-frozen.txt
./lint.py
PYTHONPATH=src pytest
```

## Plans

- Pretty soon, change to pyproject.toml.

- For 1.0.0, I want to have some comprehensive testing of input
whether that input really is ADIF, including custom fields.

## How to build?

I think I used the cooking recipe from the [packaging tutorial](https://packaging.python.org/en/latest/tutorials/packaging-projects/#uploading-the-distribution-archives), i.e.,


```
pip install --upgrade twine build
python3 -m build
python3 -m twine upload dist/*
```