# Local build and install:

## To build:

    rm -rf adif_io.egg-info build dist
    python setup.py sdist bdist_egg

Or, maybe better, in a `venv` that can take a few packages more:

    pip install --upgrade twine build
    python3 -m build

## To install:

    pip install dist/adif_io-*.tar.gz



